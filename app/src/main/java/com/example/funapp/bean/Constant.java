package com.example.funapp.bean;

/**
 * @author 李春波
 * @ProjectName FunApp
 * @PackageName：com.example.funapp.bean
 * @Description：
 * @time 2021/11/17 18:29
 */
public class Constant {

    //数据库版本号
    public static final int SCHEMA_VERSION = 1;
    //创建第一个版本数据库
    public static final String CREATE_TABLE_SQL_v1 =
            "create table note(" +
                    "note_id INTEGER primary key autoincrement, " +
                    "title TEXT, " +
                    "content TEXT, " +
                    "last_time TEXT, " +
                    "is_top text" +
                    ");";
    //删除数据库表
    public static final String DROP_TABLE_SQL = "drop table if exists student";
    //数据库名
    public static final String DB_NAME = "database.db";
    //表名
    public static final String TABLE_NAME = "note";
    //属性
    public static final String COLUMN_ID = "note_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_CONTENT = "content";
    public static final String COLUMN_LAST_TIME = "last_time";
    public static final String COLUMN_IS_TOP = "is_top";
}
