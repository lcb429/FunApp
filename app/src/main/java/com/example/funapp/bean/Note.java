package com.example.funapp.bean;

import java.io.Serializable;

/**
 * @author 李春波
 * @ProjectName FunApp
 * @PackageName：com.example.funapp.adapter
 * @Description：
 * @time 2021/11/17 15:52
 */
public class Note implements Serializable {
    private long id;
    private String title;
    private String content;
    private String lastTime;
    private int is_top;//标志位，是否置顶，1：是，0：否

    public Note() {}

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", lastTime='" + lastTime + '\'' +
                ", is_top=" + is_top +
                '}';
    }

    public int getIs_top() {
        return is_top;
    }

    public void setIs_top(int is_top) {
        this.is_top = is_top;
    }

    public String getLastTime() {
        return lastTime;
    }

    public void setLastTime(String lastTime) {
        this.lastTime = lastTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
