package com.example.funapp.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.funapp.R;
import com.example.funapp.activity.NoteDetailActivity;
import com.example.funapp.bean.Note;
import com.example.funapp.dialog.CustomDialog;
import com.example.funapp.utils.MySqliteOpenHelper;

import java.util.List;

/**
 * @author 李春波
 * @ProjectName FunApp
 * @PackageName：com.example.funapp.view
 * @Description：
 * @time 2021/11/17 15:06
 */
public class CustomAdapter extends BaseAdapter {
    private int itemLayoutId;
    private Context context;
    private List<Note> list;

    final static class ViewHolder{
        private TextView title;
        private TextView content;
        private TextView delete;
        private TextView is_top;
        private LinearLayout list_item;
    }

    public CustomAdapter() {
    }

    public CustomAdapter(int itemLayoutId, Context context, List<Note> list) {
        this.itemLayoutId = itemLayoutId;
        this.context = context;
        this.list = list;
    }

    public int getCount() {
        if(null != list){
            return list.size();
        }else{
            return 0;
        }
    }

    public Object getItem(int position) {
        if(null != list.get(position)){
            return list.get(position);
        }else {
            return null;
        }
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(null != convertView){
            viewHolder = (ViewHolder) convertView.getTag();
        }
        else{
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(itemLayoutId,null);
            viewHolder.title = convertView.findViewById(R.id.title);
            viewHolder.content = convertView.findViewById(R.id.content);
            viewHolder.delete = convertView.findViewById(R.id.tv_delete);
            viewHolder.is_top = convertView.findViewById(R.id.is_top);
            viewHolder.list_item = convertView.findViewById(R.id.list_item);

            //设置点击事件
            viewHolder.list_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent to_detail = new Intent(context, NoteDetailActivity.class);
                    to_detail.putExtra("note_data", list.get(position));
                    context.startActivity(to_detail);
                }
            });
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //设置自定义的dialog
                    CustomDialog.Builder builder = new CustomDialog.Builder(context);
                    builder.setTitle("提示");
                    builder.setMessage("确定要删除这条备忘录吗？");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            //删除数据
                            MySqliteOpenHelper mySqliteOpenHelper = MySqliteOpenHelper.getInstance(context);
                            SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
                            int result = mySqliteOpenHelper.delete(db, list.get(position).getId());
                            db.close();
                            if(result > 0){
                                list.remove(position);
                                //刷新
                                notifyDataSetChanged();
                                //提示
                                Toast.makeText(context,"删除成功",Toast.LENGTH_SHORT).show();
                                Log.i("onClick: ", "数据删除成功");
                            }else{
                                Log.i("onClick: ", "数据删除失败");
                            }

                        }
                    });
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    //展示自定义的dialog
                    builder.create().show();
                }
            });
            viewHolder.is_top.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //得到当前item数据
                    Note note = list.get(position);
                    //打开数据库
                    MySqliteOpenHelper mySqliteOpenHelper = MySqliteOpenHelper.getInstance(context);
                    SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
                    //判断当前item是否被置顶
                    switch (list.get(position).getIs_top()){
                        case 1:
                            //取消置顶
                            note.setIs_top(0);
                            int cancel_top_result = 0;
                            try {
                                cancel_top_result = mySqliteOpenHelper.update(db, note);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            db.close();
                            if(cancel_top_result > 0){
                                Log.i("onClick: ", "取消置顶成功");
                            }else{
                                Log.i("onClick: ", "取消置顶失败");
                            }
                            break;
                        case 0:
                            //置顶
                            note.setIs_top(1);
                            int top_result = 0;
                            try {
                                top_result = mySqliteOpenHelper.update(db, note);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            db.close();
                            if(top_result > 0){
                                Log.i("onClick: ", "置顶成功");
                            }else{
                                Log.i("onClick: ", "置顶失败");
                            }
                            break;
                    }
                    //处理数据，让置顶的数据排在前面
                    int tag = 0;
                    for(int i = 0; i < list.size(); i++){
                        if(list.get(i).getIs_top() == 1){
                            Note temp = list.get(i);
                            list.set(i, list.get(tag));
                            list.set(tag, temp);
                            tag++;
                        }
                    }
                    //刷新
                    notifyDataSetChanged();
                }
            });

            convertView.setTag(viewHolder);
        }

        //展示数据
        viewHolder.title.setText(list.get(position).getTitle());
        int len = list.get(position).getContent().length();
        if (len > 20){
            viewHolder.content.setText(list.get(position).getContent().substring(0,20)+"……");
        }else{
            viewHolder.content.setText(list.get(position).getContent());
        }
        if(list.get(position).getIs_top() == 1){
            viewHolder.is_top.setText("取消置顶");
            viewHolder.list_item.setBackgroundColor(context.getResources().getColor(R.color.top_item_bg));
        }else{
            viewHolder.is_top.setText("置顶");
            viewHolder.list_item.setBackgroundColor(context.getResources().getColor(R.color.normal_item_bg));
        }

        return convertView;
    }


}
