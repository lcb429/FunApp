package com.example.funapp.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.funapp.bean.Constant;
import com.example.funapp.bean.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 李春波
 * @ProjectName FunApp
 * @PackageName：com.example.funapp.utils
 * @Description：数据库工具类
 * @time 2021/11/17 17:53
 */
public class MySqliteOpenHelper extends SQLiteOpenHelper {
    private static MySqliteOpenHelper instance;

    /**
     * 构造方法不对外暴露
     * @param context
     * @param name
     * @param factory
     * @param version
     */
    private MySqliteOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * 单例模式
     * @param context 传入上下文
     * @return 返回MySQLiteOpenHelper对象
     */
    public static MySqliteOpenHelper getInstance(Context context) {
        if (null == instance) {
            synchronized (MySqliteOpenHelper.class) {
                if (null == instance) {
                    instance = new MySqliteOpenHelper(context, Constant.DB_NAME, null, Constant.SCHEMA_VERSION);
                }
            }
        }
        return instance;
    }

    /**
     * 创建数据库、表，初始化数据
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Constant.CREATE_TABLE_SQL_v1);
    }

    /**
     * 升级判断,如果再升级就要再加两个判断,从1到3,从2到3
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion==1 && newVersion==2) {
            db.execSQL("ALTER TABLE restaurants ADD phone TEXT;");
        }
    }

    /**
     * 数据库降级
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    /**
     * 获取全部数据
     * @return
     */
    public List<Note> queryAll(SQLiteDatabase db) throws Exception {
        Cursor cursor = db.query(Constant.TABLE_NAME, null, null, null, null, null, null, null);
        List<Note> notes = new ArrayList<>();
        while (cursor.moveToNext()) {
            Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constant.COLUMN_ID)));
            String title = cursor.getString(cursor.getColumnIndex(Constant.COLUMN_TITLE));
            String content = cursor.getString(cursor.getColumnIndex(Constant.COLUMN_CONTENT));
            String lastTime = cursor.getString(cursor.getColumnIndex(Constant.COLUMN_LAST_TIME));
            int isTop = Integer.parseInt(cursor.getString((cursor.getColumnIndex(Constant.COLUMN_IS_TOP))));
            Note note = new Note();
            note.setId(id.longValue());
            note.setTitle(title);
            note.setContent(content);
            note.setLastTime(lastTime);
            note.setIs_top(isTop);
            notes.add(decrypeNote(note));
        }
        return notes;
    }

    /**
     * 根据id查询数据库
     * @param select_id
     * @return
     */
    public List<Note> queryById(SQLiteDatabase db, long select_id) throws Exception {
        Cursor cursor = db.query(Constant.TABLE_NAME, null, Constant.COLUMN_ID + "=" + select_id, null, null, null, null);
        List<Note> notes = new ArrayList<>();
        while (cursor.moveToNext()) {
            Integer id = Integer.parseInt(cursor.getString(cursor.getColumnIndex(Constant.COLUMN_ID)));
            String title = cursor.getString(cursor.getColumnIndex(Constant.COLUMN_TITLE));
            String content = cursor.getString(cursor.getColumnIndex(Constant.COLUMN_CONTENT));
            String lastTime = cursor.getString(cursor.getColumnIndex(Constant.COLUMN_LAST_TIME));
            int isTop = Integer.parseInt(cursor.getString((cursor.getColumnIndex(Constant.COLUMN_IS_TOP))));
            Note note = new Note();
            note.setId(id.longValue());
            note.setTitle(title);
            note.setContent(content);
            note.setLastTime(lastTime);
            note.setIs_top(isTop);
            notes.add(decrypeNote(note));
        }
        return notes;
    }

    /**
     * 插入新的数据，并返回插入新数据的id
     * @param note
     * @return
     */
    public long insert(SQLiteDatabase db, Note note) throws Exception {
        // 加密
        Note enNote = encrypeNote(note);
        // 数据
        ContentValues cv = new ContentValues();
        cv.put(Constant.COLUMN_TITLE, enNote.getTitle());
        cv.put(Constant.COLUMN_CONTENT, enNote.getContent());
        cv.put(Constant.COLUMN_LAST_TIME, enNote.getLastTime());
        cv.put(Constant.COLUMN_IS_TOP,enNote.getIs_top());
        //得到插入后的数据id
        long new_id = db.insert(Constant.TABLE_NAME, null, cv);
        cv.clear();
        return new_id;
    }

    /**
     * 更新数据，并返回受影响的条数
     * @param note
     * @return
     */
    public int update(SQLiteDatabase db, Note note) throws Exception {
        String[] args = {note.getId()+""};
        // 加密
        Note enNote = encrypeNote(note);
        // 数据
        ContentValues cv = new ContentValues();
        cv.put(Constant.COLUMN_TITLE, enNote.getTitle());
        cv.put(Constant.COLUMN_CONTENT, enNote.getContent());
        cv.put(Constant.COLUMN_LAST_TIME, enNote.getLastTime());
        cv.put(Constant.COLUMN_IS_TOP,enNote.getIs_top());
        //得到受影响的数据条数
        int result = db.update(Constant.TABLE_NAME, cv, Constant.COLUMN_ID+"=?", args);
        cv.clear();
        return result;
    }

    /**
     * 根据id删除数据，并返回受影响的条数
     * @param delete_id
     */
    public int delete(SQLiteDatabase db, long delete_id){
        //得到受影响的数据条数
        int result = db.delete(Constant.TABLE_NAME, Constant.COLUMN_ID+"=?" , new String[]{delete_id+""});
        return  result;
    }

    /**
     * 将Note里面的数据进行ECB加密
     * @param note
     * @return
     */
    public Note encrypeNote(Note note) throws Exception {
        Note en_note = new Note();
        en_note.setId(note.getId());
        en_note.setTitle(ECB.des3EncodeECB(note.getTitle()));
        en_note.setContent(ECB.des3EncodeECB(note.getContent()));
        en_note.setLastTime(ECB.des3EncodeECB(note.getLastTime()));
        en_note.setIs_top(note.getIs_top());

        Log.i("encrypeNote: ", en_note.toString());

        return en_note;
    }

    /**
     * 将Note里面的数据进行ECB解密
     * @param note
     * @return
     */
    public Note decrypeNote(Note note) throws Exception {
        Note de_note = new Note();
        de_note.setId(note.getId());
        de_note.setTitle(ECB.des3DecodeECB(note.getTitle()));
        de_note.setContent(ECB.des3DecodeECB(note.getContent()));
        de_note.setLastTime(ECB.des3DecodeECB(note.getLastTime()));
        de_note.setIs_top(note.getIs_top());

        Log.i("decrypeNote: ", de_note.toString());

        return de_note;
    }


}
