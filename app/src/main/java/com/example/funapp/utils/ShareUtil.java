package com.example.funapp.utils;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import androidx.core.content.FileProvider;

/**
 * @author 李春波
 * @ProjectName FunApp
 * @PackageName：com.example.funapp.utils
 * @Description：分享工具类
 * @time 2021/12/7 18:35
 */
public class ShareUtil {
    private Context context; //Activity.this 的 context 返回当前activity的上下文,属于activity ,activity 摧毁他就摧毁
    private Context applicationContext; //应用的上下文，生命周期是整个应用，应用摧毁它才摧毁

    public ShareUtil(){}

    /**
     * 构造方法
     * @param context Activity.this 的 context
     */
    public ShareUtil(Context context) {
        this.context = context;
    }

    /**
     * 构造方法
     * @param context            Activity.this 的 context
     * @param applicationContext 应用的上下文，生命周期是整个应用
     */
    public ShareUtil(Context context, Context applicationContext){
        this.context = context;
        this.applicationContext = applicationContext;
    }

    // 简介
    // 文字 & qq
    // ShareUtil.shareText("com.tencent.mobileqq", null, "内容", "分享标题", "分享主题");
    // 文字 & 微信
    // ShareUtil.shareText("com.tencent.mm", null, "内容", "分享标题", "分享主题");
    // 文字 & 所有app
    // ShareUtil.shareText(null, null, "内容", "分享标题", "分享主题");
    // 图片 & 微信好友
    // ShareUtil.shareImg("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI", fileImage);
    // 图片 & qq好友
    // ShareUtil.shareImg("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity", fileImage);
    // 音乐 & 微信好友
    // ShareUtil.shareAudio("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI", fileAudio);
    // 音乐 & qq好友
    // ShareUtil.shareAudio("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity", fileAudio);
    // 视频 & 微信好友
    // ShareUtil.shareVideo("com.tencent.mm", "com.tencent.mm.ui.tools.ShareImgUI", fileVideo);
    // 视频 & qq好友
    // ShareUtil.shareVideo("com.tencent.mobileqq", "com.tencent.mobileqq.activity.JumpActivity", fileVideo);

    /**
     * 分享文字
     * @param packageName 要分享的app, null代表全部
     * @param className   要分享app的Activity, null代表全部
     * @param content     要分享的内容
     * @param title       要分享的标题
     * @param subject     要分享的主题
     */
    public void shareText(String packageName, String className, String content, String title, String subject){
        Intent intent =new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        if(stringCheck(className) && stringCheck(packageName)){
            ComponentName componentName = new ComponentName(packageName, className);
            intent.setComponent(componentName);
        }else if(stringCheck(packageName)){
            intent.setPackage(packageName);
        }

        intent.putExtra(Intent.EXTRA_TEXT, content);
        if(null != title && !TextUtils.isEmpty(title)){
            intent.putExtra(Intent.EXTRA_TITLE, title);
        }
        if(null != subject && !TextUtils.isEmpty(subject)){
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        }
        intent.putExtra(Intent.EXTRA_TITLE, title);
        Intent chooserIntent = Intent.createChooser(intent, "分享到：");
        context.startActivity(chooserIntent);
    }

    /**
     * 分享图片
     * @param packageName 要分享的app, null代表全部
     * @param className   要分享的app的Activity, null代表全部
     * @param file        要分享的文件
     */
    public void shareImg(String packageName, String className, File file){
        setIntent("image/*", packageName, className, file);
    }

    /**
     * 分享音乐
     * @param packageName 要分享的app, null代表全部
     * @param className   要分享的app的Activity, null代表全部
     * @param file        要分享的文件
     */
    public void shareAudio(String packageName, String className, File file){
        setIntent("audio/*", packageName, className, file);
    }

    /**
     * 分享视频
     * @param packageName 要分享的app, null代表全部
     * @param className   要分享的app的Activity, null代表全部
     * @param file        要分享的文件
     */
    public void shareVideo(String packageName, String className, File file){
        setIntent("video/*", packageName, className, file);
    }

    /**
     * 分享文件
     * @param type        要分享的文件类型
     * @param packageName 要分享的app, null代表全部
     * @param className   要分享的app的Activity, null代表全部
     * @param file        要分享的文件
     */
    public void setIntent(String type, String packageName, String className, File file){
        if(file.exists()){
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {// 7.0及以上需要使用fileProvider方式
                String authority = applicationContext.getPackageName() + ".fileProvider";//确保authority与AndroidManifest.xml中android:authorities="包名.fileProvider"所有字符一致
                uri = FileProvider.getUriForFile(context, authority, file);//需要访问内存里的文件,所以要用FileProvider解决应用程序将file://Uri暴露给另一个应用程序时引发的FileUriExposedException异常
            } else {
                uri = Uri.fromFile(file);
            }
            if(uri != null){
                Log.i("FileProvider替换前的路径", "file://"+file.getAbsolutePath());
                Log.i("FileProvider替换后的路径", uri+"");
            }else {
                Log.e("setIntent: ", "Uri为空");
                return;
            }
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//调用Intent.setFlags()来设置权限权限失效的时机：接收Intent的Activity所在的堆销毁时
            intent.setType(type);
            if(stringCheck(packageName) && stringCheck(className)){
                intent.setComponent(new ComponentName(packageName, className));
            }else if (stringCheck(packageName)) {
                intent.setPackage(packageName);
            }
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            Intent chooserIntent = Intent.createChooser(intent, "分享到:");
            context.startActivity(chooserIntent);
        }else {
            Toast.makeText(context, "文件不存在", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 判断字符串是否存在
     * @param str 输入的字符串
     * @return    true代表字符串存在, false代表字符串不存在
     */
    public static boolean stringCheck(String str){
        if(null != str && !TextUtils.isEmpty(str)){
            return true;
        }else {
            return false;
        }
    }
}
