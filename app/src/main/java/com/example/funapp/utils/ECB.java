package com.example.funapp.utils;

import android.util.Base64;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;


/**
 * @author 李春波
 * @ProjectName FunApp
 * @PackageName：com.example.funapp.utils
 * @Description：加密解密持久化到数据库的数据
 * @time 2021/12/5 17:16
 */

public class ECB {
    /**
     * @param plain_str 明文
     * @return Base64编码的密文
     * @throws Exception
     * @Description ECB加密，不要IV
     */
    public static String des3EncodeECB(String plain_str) throws Exception {
        // 获取明文并转化成byte
        byte[] data = plain_str.getBytes("UTF-8");
        // 获取密钥工厂类对象，DESede的秘钥(192位)要比DES的秘钥(64位)长
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        // 设置秘钥
        byte[] key = Base64.decode("YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4".getBytes(), Base64.DEFAULT);
        // 设置密钥参数
        DESedeKeySpec keySpec = new DESedeKeySpec(key);
        // 得到密钥对象
        Key desKey = keyFactory.generateSecret(keySpec);
        // 返回实现指定转换的Cipher对象
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        // 用密钥初始化此Cipher
        cipher.init(Cipher.ENCRYPT_MODE, desKey);
        // 按单部分操作加密或解密数据，或者结束一个多部分操作
        byte[] bOut = cipher.doFinal(data);
        // 使用ECB加密后再Base64进行一次加密
        String cipher_str = new String(Base64.encode(bOut, Base64.DEFAULT),"UTF-8");

        return cipher_str;
    }

    /**
     * @param cipher_str Base64编码的密文
     * @return 明文
     * @throws Exception
     * @Description ECB解密，不要IV
     */
    public static String  des3DecodeECB(String cipher_str) throws Exception {
        // 密文先用Base64解一次密，再用ECB解密
        byte[] data = Base64.decode(cipher_str,  Base64.DEFAULT);
        // 获取密钥工厂类对象，DESede的秘钥(192位)要比DES的秘钥(64位)长
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        // 设置秘钥
        byte[] key = Base64.decode("YWJjZGVmZ2hpamtsbW5vcHFyc3R1dnd4".getBytes(), Base64.DEFAULT);
        // 设置密钥参数
        DESedeKeySpec keySpec = new DESedeKeySpec(key);
        // 得到密钥对象
        Key desKey = keyFactory.generateSecret(keySpec);
        // 返回实现指定转换的Cipher对象
        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        // 用密钥初始化此Cipher
        cipher.init(Cipher.DECRYPT_MODE, desKey);
        // 按单部分操作加密或解密数据，或者结束一个多部分操作
        byte[] bOut = cipher.doFinal(data);
        // 将byte类型转换成String
        String plain_str = new String(bOut, "UTF-8");

        return plain_str;
    }
}
