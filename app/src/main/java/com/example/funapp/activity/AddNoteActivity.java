package com.example.funapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.funapp.R;
import com.example.funapp.bean.Note;
import com.example.funapp.dialog.CustomDialog;
import com.example.funapp.utils.MySqliteOpenHelper;
import com.example.funapp.utils.SystemUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class AddNoteActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private EditText title;
    private EditText content;
    private TextView tvWrite;

    private MySqliteOpenHelper mySqliteOpenHelper;
    private CustomTextChangedListener customTextChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        //设置沉浸式状态栏
        SystemUtil.fullScreen(AddNoteActivity.this);

        //获取视图控件
        initView();

        //为自定义的toolbar设置menu
        toolbar.inflateMenu(R.menu.add_note_toolbar_menu);

        //设置焦点
        content.requestFocus();

        //设置监听器
        setListener();

    }

    /**
     * 获取视图控件
     */
    public void initView(){
        toolbar = (Toolbar) findViewById(R.id.add_note_toolbar);
        title = (EditText) findViewById(R.id.editTextTitle);
        content = (EditText) findViewById(R.id.editTextContent);
        tvWrite = (TextView) findViewById(R.id.tv_write_num);
    }

    /**
     * 设置监听器
     */
    public void setListener(){
        //注册输入字符监听器
        customTextChangedListener = new CustomTextChangedListener();
        content.addTextChangedListener(customTextChangedListener);

        //为toolbar的menu设置点击事件，保存信息
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.save_note:
                        String title_text = title.getText().toString().trim();
                        String content_text = content.getText().toString().trim();
                        //判断是否输入了内容
                        if (title.length()!=0 || content.length()!=0) {
                            Note note = new Note();
                            //获取当前时间
                            Date date = new Date();
                            DateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                            String lastTime = format.format(date);
                            //为note赋值
                            note.setTitle(title_text);
                            note.setContent(content_text);
                            note.setLastTime(lastTime);
                            note.setIs_top(0);
                            //插入数据
                            mySqliteOpenHelper = MySqliteOpenHelper.getInstance(AddNoteActivity.this);
                            SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
                            Long new_id = null;
                            try {
                                new_id = mySqliteOpenHelper.insert(db, note);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            db.close();
                            if(new_id != null) {
                                Log.i("onClick：","数据插入成功，新id："+new_id);
                            }else{
                                Log.i("onClick：","数据插入失败");
                            }
                            //跳转
                            Intent to_main = new Intent(AddNoteActivity.this, MainActivity.class);
                            startActivity(to_main);
                            finish();
                        }else{
                            Toast.makeText(AddNoteActivity.this,"您未输入内容", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                return true;
            }
        });

        //返回主页面
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title_text = title.getText().toString().trim();
                String content_text = content.getText().toString().trim();
                Intent to_main = new Intent(AddNoteActivity.this, MainActivity.class);
                //如果写入了数据
                if(title_text.length()>0 || content_text.length()>0){
                    //设置自定义的dialog
                    CustomDialog.Builder builder = new CustomDialog.Builder(AddNoteActivity.this);
                    builder.setTitle("提示");
                    builder.setMessage("要保存输入的内容吗？");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            //判断是否输入了内容
                            if(title.length()!=0 || content.length()!=0){
                                Note note = new Note();
                                //获取当前时间
                                Date date = new Date();
                                DateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                String lastTime = format.format(date);
                                //为note赋值
                                note.setTitle(title_text);
                                note.setContent(content_text);
                                note.setLastTime(lastTime);
                                note.setIs_top(0);
                                //插入数据
                                mySqliteOpenHelper = MySqliteOpenHelper.getInstance(AddNoteActivity.this);
                                SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
                                Long new_id = null;
                                try {
                                    new_id = mySqliteOpenHelper.insert(db, note);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                db.close();
                                if(new_id != null) {
                                    Log.i("onClick：","数据插入成功，新id："+new_id);
                                }else{
                                    Log.i("onClick：","数据插入失败");
                                }
                            }
                            startActivity(to_main);
                            finish();
                        }
                    });
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            startActivity(to_main);
                            finish();
                        }
                    });
                    //展示自定义的dialog
                    builder.create().show();
                }else{
                    startActivity(to_main);
                    finish();
                }

            }
        });
    }

    /**
     * editText字符输入监听器
     */
    class CustomTextChangedListener implements TextWatcher {
        private CharSequence temp;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            temp = s;
        }

        @Override
        public void afterTextChanged(Editable s) {
            tvWrite.setText("已输入" + temp.length() + "字符");
        }
    }
}