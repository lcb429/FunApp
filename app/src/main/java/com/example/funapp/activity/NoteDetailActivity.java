package com.example.funapp.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.funapp.R;
import com.example.funapp.bean.Note;
import com.example.funapp.dialog.CustomDialog;
import com.example.funapp.utils.MySqliteOpenHelper;
import com.example.funapp.utils.PermissionUtil;
import com.example.funapp.utils.ShareUtil;
import com.example.funapp.utils.SystemUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class NoteDetailActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView last_time;
    private EditText title;
    private EditText content;
    private TextView write_num;

    private View curView;

    private Note data;

    private MySqliteOpenHelper mySqliteOpenHelper;
    private CustomTextChangedListener customTextChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);

        //设置沉浸式状态栏
        SystemUtil.fullScreen(NoteDetailActivity.this);

        //获取视图控件
        initView();

        //获取传过来的数据
        getData();

        //显示数据
        showData();

        //为自定义的toolbar设置menu
        toolbar.inflateMenu(R.menu.note_detail_toolbar);

        //注册监听器
        setListener();

    }

    /**
     * 获取传过来的数据
     */
    public void getData(){
        data = (Note) getIntent().getSerializableExtra("note_data");
    }

    /**
     * 显示数据
     */
    public void showData(){
        last_time.setText(data.getLastTime());
        title.setText(data.getTitle());
        content.setText(data.getContent());
        write_num.setText("共 " + data.getContent().length() + " 字");
    }

    /**
     * 获取视图控件
     */
    public void initView(){
        curView = getWindow().getDecorView();
        toolbar = (Toolbar) findViewById(R.id.note_detail_toolbar);
        last_time = (TextView) findViewById(R.id.tx_last_time);
        title = (EditText) findViewById(R.id.title_detail);
        content = (EditText) findViewById(R.id.content_detail);
        write_num = (TextView) findViewById(R.id.write_num_detail);
    }

    /**
     * 注册监听器
     */
    public void setListener(){
        //注册输入字符监听器
        customTextChangedListener = new CustomTextChangedListener();
        content.addTextChangedListener(customTextChangedListener);

        //为toolbar的menu设置点击事件，保存信息
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    //保存note
                    case R.id.save_note:
                        String title_text = title.getText().toString().trim();
                        String content_text = content.getText().toString().trim();
                        Intent to_main = new Intent(NoteDetailActivity.this, MainActivity.class);
                        //如果数据改变了，就更新
                        if (title_text.length()!=data.getTitle().length() || content_text.length()!=data.getContent().length()) {
                            Note note = new Note();
                            //获取当前时间
                            DateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                            Date date = new Date();
                            String lastTime = format.format(date);
                            //为note赋值
                            note.setId(data.getId());
                            note.setTitle(title_text);
                            note.setContent(content_text);
                            note.setLastTime(lastTime);
                            note.setIs_top(data.getIs_top());
                            //更新数据
                            mySqliteOpenHelper =  MySqliteOpenHelper.getInstance(NoteDetailActivity.this);
                            SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
                            //标题和内容都为空，删除数据
                            if(title.length()==0 && content.length()==0){
                                int result = mySqliteOpenHelper.delete(db, data.getId());
                                db.close();
                                if(result > 0){
                                    Log.i("onClick: ", "数据删除成功");
                                }else{
                                    Log.i("onClick: ", "数据删除失败");
                                }
                                startActivity(to_main);
                                finish();
                            }else{//更新数据
                                int change = 0;
                                try {
                                    change = mySqliteOpenHelper.update(db, note);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                db.close();
                                if(change > 0) {
                                    Log.i("onClick：","数据更新成功");
                                }else{
                                    Log.i("onClick：","数据更新失败");
                                }
                                startActivity(to_main);
                                finish();
                            }
                        }else{
                            startActivity(to_main);
                            finish();
                        }
                        break;
                    //分享为文本,不需要访问内存里的文件,所以用不到FileProvider
                    case R.id.share_with_text:
                        ShareUtil swt = new ShareUtil(NoteDetailActivity.this);
                        swt.shareText(null, null, "标题："+data.getTitle()+"\n"+"内容："+data.getContent(), data.getTitle()+"", "subject");
                        break;
                    //分享为图片,需要访问内存里的文件,所以要用FileProvider解决应用程序将file://Uri暴露给另一个应用程序时引发的FileUriExposedException异常
                    case R.id.share_with_img:
                        //检测读写权限
                        PermissionUtil.verifyStoragePermissions(NoteDetailActivity.this);
                        File file = saveShareImg(curView, "share");
                        if(file != null){
                            // getApplicationContext() 返回应用的上下文,生命周期是整个应用,应用摧毁它才摧毁
                            // Activity.this 的 context 返回当前activity的上下文,属于activity ,activity 摧毁他就摧毁
                            // getBaseContext() 返回由构造函数指定或setBaseContext()设置的上下文
                            // this.getApplicationContex()取的是这个应用程序的Context,Activity.this取的是这个Activity的Context,这两者的生命周期是不同的,前者的生命周期是整个应用,后者的生命周期只是它所在的Activity
                            ShareUtil swi = new ShareUtil(NoteDetailActivity.this, getApplicationContext());
                            swi.shareImg(null, null, file);
                            getApplicationContext();
                        }else {
                            Toast.makeText(NoteDetailActivity.this, "分享失败", Toast.LENGTH_SHORT).show();
                        }
                        break;
                }
                return true;
            }
        });

        //返回主页面
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title_text = title.getText().toString().trim();
                String content_text = content.getText().toString().trim();
                Intent to_main = new Intent(NoteDetailActivity.this, MainActivity.class);
                //如果数据改变了
                if(title_text.length()!=data.getTitle().length() || content_text.length()!=data.getContent().length()){
                    //设置自定义的dialog
                    CustomDialog.Builder builder = new CustomDialog.Builder(NoteDetailActivity.this);
                    builder.setTitle("提示");
                    builder.setMessage("要保存更新的内容吗？");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            //打开数据库
                            mySqliteOpenHelper = MySqliteOpenHelper.getInstance(NoteDetailActivity.this);
                            SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
                            //判断是否输入了内容
                            if(title.length()!=0 || content.length()!=0){
                                Note note = new Note();
                                //获取当前时间
                                Date date = new Date();
                                DateFormat format = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
                                String lastTime = format.format(date);
                                //为note赋值
                                note.setId(data.getId());
                                note.setTitle(title_text);
                                note.setContent(content_text);
                                note.setLastTime(lastTime);
                                note.setIs_top(data.getIs_top());
                                //更新数据
                                int change = 0;
                                try {
                                    change = mySqliteOpenHelper.update(db, note);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                db.close();
                                if(change > 0) {
                                    Log.i("onClick：","数据更新成功");
                                }else{
                                    Log.i("onClick：","数据更新失败");
                                }
                            }
                            if(title.length()==0 && content.length()==0){
                                int result = mySqliteOpenHelper.delete(db, data.getId());
                                db.close();
                                if(result > 0){
                                    Log.i("onClick: ", "数据删除成功");
                                }else{
                                    Log.i("onClick: ", "数据删除失败");
                                }
                            }
                            startActivity(to_main);
                            finish();
                        }
                    });
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            startActivity(to_main);
                            finish();
                        }
                    });
                    //展示自定义的dialog
                    builder.create().show();
                }else{
                    startActivity(to_main);
                    finish();
                }
            }
        });
    }

    /**
     * 自定义editText输入字符监听器
     */
    class CustomTextChangedListener implements TextWatcher {
        private CharSequence temp;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            temp = s;
        }

        @Override
        public void afterTextChanged(Editable s) {
            write_num.setText("共 " + temp.length() + " 字");
        }
    }

    /**
     * View转Bitmap
     * @param v 当前视图view
     * @return
     */
    public Bitmap viewToBitmap(View v) {
        //获取视图View的高和宽
        int width = v.getWidth();
        int height = v.getHeight();
        //生成一个ARG_B8888的bitmap,宽度和高度为传入view的宽高,这是初始的图像，需要裁剪
        Bitmap source = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //根据bitmap生成一个画布
        Canvas canvas = new Canvas(source);
        //注意：这里是解决图片透明度问题，给底色上白色，不然是透明色背景可能会很难看，若存储时保存的为png格式的图，则无需此步骤
        canvas.drawColor(Color.WHITE);
        //绘图
        v.draw(canvas);
        //裁剪已有的bitmap
        int newX = 0;
        int newY = toolbar.getHeight();
        int newWidth = width;
        int newHeight = height - toolbar.getHeight() - getNavigationBarHeight();
        Bitmap bitmap = Bitmap.createBitmap(source, newX, newY, newWidth, newHeight);

        return bitmap;
    }

    /**
     * 获取顶部状态栏高度
     * @return
     */
    private int getStatusBarHeight() {
        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier("status_bar_height", "dimen","android");
        int height = resources.getDimensionPixelSize(resourceId);
        Log.i("状态栏高度", height+"");
        return height;
    }

    /**
     * 获取底部导航栏的高度
     * @return
     */
    private int getNavigationBarHeight() {
        if(checkDeviceHasNavigationBar(this)){
            Resources resources = this.getResources();
            int resourceId = resources.getIdentifier("navigation_bar_height","dimen", "android");
            int height = resources.getDimensionPixelSize(resourceId);
            Log.i("导航栏高度", height+"");
            return height;
        }else{
            return 0;
        }
    }

    /**
     * 判断设备是否存在导航栏
     * @param context 上下文环境
     * @return
     */
    public static boolean checkDeviceHasNavigationBar(Context context) {
        boolean hasNavigationBar = false;
        Resources rs = context.getResources();
        int id = rs.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            hasNavigationBar = rs.getBoolean(id);
        }
        try {
            Class systemPropertiesClass = Class.forName("android.os.SystemProperties");
            Method m = systemPropertiesClass.getMethod("get", String.class);
            String navBarOverride = (String) m.invoke(systemPropertiesClass, "qemu.hw.mainkeys");
            if ("1".equals(navBarOverride)) {
                hasNavigationBar = false;
            } else if ("0".equals(navBarOverride)) {
                hasNavigationBar = true;
            }
        } catch (Exception e) {

        }
        return hasNavigationBar;

    }

    /**
     * 把从View转的图片保存到本地
     * @param view    当前视图view
     * @param imgName 要保存的图片名
     * @return        返回生成的文件
     */
    public File saveShareImg(View view, String imgName) {
        Bitmap bitmap = viewToBitmap(view);
        try {
            //Environment.getExternalStorageDirectory()是外部存储/storage/emulated/0,相当于手机内部存储空间的根目录
            String appHomeDir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/FunApp/Share";
            //如果目录不存在就新建目录
            File filePath = new File(appHomeDir);
            if (!filePath.exists()) {
                boolean mkdirs = filePath.mkdirs();
                if (!mkdirs) {
                    Log.i("TAG", "文件夹创建失败");
                } else {
                    Log.i("TAG", "文件夹创建成功");
                }
            }
            //新建文件
            File file = new File(filePath+"/"+imgName+".jpg");
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();
            bitmap.recycle();//及时回收Bitmap对象，防止OOM
            view.destroyDrawingCache();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}