package com.example.funapp.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.example.funapp.R;
import com.example.funapp.adapter.CustomAdapter;
import com.example.funapp.bean.Note;
import com.example.funapp.utils.MySqliteOpenHelper;
import com.example.funapp.utils.PermissionUtil;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
    private List<Note> list;
    private ListView listView;
    private Toolbar toolbar;
    private EditText edit_search;

    private MySqliteOpenHelper mySqliteOpenHelper;
    private CustomTextChangedListener customTextChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //设置沉浸式状态栏
        //SystemUtil.fullScreen(MainActivity.this);

        //检测读写权限
        PermissionUtil.verifyStoragePermissions(this);

        //从数据库读取数据
        getData();

        //处理数据，让置顶的数据排在前面
        initData();

        //获取视图控件
        initView();

        //为自定义的toolbar设置menu
        toolbar.inflateMenu(R.menu.main_toolbar_menu);

        //为toolbar的menu设置点击事件
        setListener();

        //展示数据
        if(list!=null && list.size()>0){
            CustomAdapter adapter = new CustomAdapter(R.layout.custom_item, this, list);
            listView.setAdapter(adapter);
        }
    }

    /**
     * 从数据库读取数据
     */
    public void getData(){
        mySqliteOpenHelper = MySqliteOpenHelper.getInstance(MainActivity.this);
        SQLiteDatabase db = mySqliteOpenHelper.getReadableDatabase();
        try {
            list = mySqliteOpenHelper.queryAll(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
        db.close();
    }

    /**
     * 获取控件
     */
    public void initView(){
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        listView = (ListView) findViewById(R.id.list_view);
        edit_search = (EditText) findViewById(R.id.edit_search);
    }

    /**
     * 处理数据，让置顶的数据排在前面
     */
    public void initData(){
        if (list != null){
            int tag = 0;
            for(int i = 0; i < list.size(); i++){
                if(list.get(i).getIs_top() == 1){
                    Note temp = list.get(i);
                    list.set(i, list.get(tag));
                    list.set(tag, temp);
                    tag++;
                }
            }
        }
    }

    /**
     * 设置点击事件监听器
     */
    public void setListener(){
        //设置字符监听器
        customTextChangedListener = new CustomTextChangedListener();
        edit_search.addTextChangedListener(customTextChangedListener);

        //toolbar点击事件
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.add_note:
                        Intent to_add = new Intent(MainActivity.this, AddNoteActivity.class);
                        startActivity(to_add);
                        finish();
                        break;
                    case R.id.color1:

                        break;
                    case R.id.color2:

                        break;
                }
                return true;
            }
        });
    }

    /**
     * editText字符输入监听器
     */
    class CustomTextChangedListener implements TextWatcher {
        private CharSequence temp;

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            temp = s;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if(temp.length() == 0){
                CustomAdapter adapter = new CustomAdapter(R.layout.custom_item, MainActivity.this, list);
                listView.setAdapter(adapter);
            }else{
                String str = edit_search.getText().toString().trim();
                List<Note> temp = new ArrayList<Note>();
                if(str.length() > 0){
                    //得到符合条件的数据
                    for (int i = 0; i < list.size(); i++) {
                        int j = list.get(i).getTitle().indexOf(str);
                        int k = list.get(i).getContent().indexOf(str);
                        if(j!=-1 || k!=-1){
                            temp.add(list.get(i));
                        }
                    }
                    CustomAdapter adapter = new CustomAdapter(R.layout.custom_item, MainActivity.this, temp);
                    listView.setAdapter(adapter);
                }
            }
        }
    }

}